import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import axios from "axios";
import LinkRoute from "../components/common/LinkRoute";
import Delete from "./Delete";
import Header from "../components/common/Header";

const DashboardStyled = styled.div`
  // background-color: #9ca1ae;
  padding: 25px;
  width: 100%;
  height: auto;
  padding-left: 72px;
  // padding-top: 170px;
  position: absolute;
  top: 140px;
`;

const WrapperStyled = styled.div`
  background-color: #fff;
  padding: 20px;
  overflow-x: scroll;
`;

const TableStyled = styled.table`
  width: 100% !important;
  overflow: hidden !important;
  overflow: x-scroll;

  .action-class {
    padding: 2px;
  }
  .view {
    color: #4dd34d;
  }
  .edit {
    color: #c4c417;
  }
  .delete {
    color: red;
  }
`;

function Home() {
  const [users, setUsers] = useState([]);

  const URL = "https://crud-backend-ga3x.onrender.com/";

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios(URL);
        console.log(response.data);
        setUsers(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  const donothing = () => {};

  return (
    <>
      <Header text="All Users" />
      <DashboardStyled>
        <WrapperStyled>
          <TableStyled className="container table table-striped">
            {/* <table className="container table table-striped"> */}
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">First</th>
                <th scope="col">Phone</th>
                <th scope="col">Email</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              {users.map((ele) => {
                return (
                  <tr>
                    <th scope="row">
                      <LinkRoute to={"userbio/" + ele.id}>{ele.id}</LinkRoute>
                    </th>

                    <td>
                      <LinkRoute to={"userbio/" + ele.id}>{ele.name}</LinkRoute>
                    </td>

                    <td>
                      <LinkRoute to={"userbio/" + ele.id}>
                        {ele.phone}
                      </LinkRoute>
                    </td>

                    <td>
                      <LinkRoute to={"userbio/" + ele.id}>
                        {ele.email}
                      </LinkRoute>
                    </td>

                    <td>
                      <LinkRoute to={"userbio/" + ele.id}>
                        <span className="material-symbols-outlined action-class view">
                          table_view
                        </span>
                      </LinkRoute>

                      <LinkRoute to={"/editusers/" + ele.id}>
                        <span className="material-symbols-outlined action-class edit">
                          edit
                        </span>
                      </LinkRoute>

                      <Delete
                        id={ele.id}
                        className="material-symbols-outlined action-class delete"
                      />
                    </td>
                  </tr>
                );
              })}
            </tbody>
            {/* </table> */}
          </TableStyled>
        </WrapperStyled>
      </DashboardStyled>
    </>
  );
}

export default Home;
