import React, { useState } from "react";
import axios from "axios";
import styled from "@emotion/styled";
import ButtonComponent from "../components/common/ButtonComponent";
import HomeRedirection from "../components/common/HomeRedirection";
import Header from "../components/common/Header";

const DashboardStyled = styled.div`
  position: absolute;
  // background-color: #9ca1ae;
  padding: 25px;
  height: auto;
  width: 100%;
  padding-left: 72px;
  // padding-top: 170px;
  top: 140px;

  .action-class {
    padding: 2px;
  }
  .edit {
    color: #c4c417;
  }
  .delete {
    color: red;
  }

  .form-wrapper {
    background-color: #fff;
    padding: 40px;
  }
`;

const URL = "https://crud-backend-ga3x.onrender.com/create";

function Create() {
  const [adduser, setAddUser] = useState([
    {
      name: "",
      phone: "",
      email: "",
    },
  ]);

  const handlechange = (e) => {
    var name = e.target.name;
    var value = e.target.value;

    setAddUser({ ...adduser, [name]: value });
    // console.log(adduser);
  };

  const handleonsubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(URL, adduser);
      console.log(response);
      window.location = "/";
    } catch (error) {
      console.log(error);
    }
  };

  const donothing = () => {};

  return (
    <>
      <Header text="Add User" />
      <DashboardStyled>
        <HomeRedirection />
        <form className=" form-wrapper" onSubmit={handleonsubmit}>
          <div className="row">
            <label htmlFor="name"> Enter Your Name </label>
            <input
              type="text"
              className="form-control"
              placeholder="First name"
              onChange={handlechange}
              name="name"
            />
          </div>
          <div className="row">
            <label htmlFor="Phone"> Enter Your Phone Number </label>
            <input
              type="number"
              className="form-control"
              placeholder="Phone Number"
              onChange={handlechange}
              name="phone"
            />
          </div>
          <div className="row">
            <label htmlFor="Email"> Enter Your Email ID </label>
            <input
              type="email"
              className="form-control"
              placeholder="Email ID"
              onChange={handlechange}
              name="email"
            />
          </div>
          <ButtonComponent
            type="submit"
            border="0"
            bcolor="blue"
            borderradius="4px"
            margin="14px"
            padding="6px 34px"
            color="#fff"
            onClick={donothing}
          >
            Submit
          </ButtonComponent>
        </form>
      </DashboardStyled>
    </>
  );
}

export default Create;
