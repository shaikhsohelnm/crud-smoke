import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import styled from "@emotion/styled";
import ButtonComponent from "../components/common/ButtonComponent";
import HomeRedirection from "../components/common/HomeRedirection";
import Header from "../components/common/Header";

const DashboardStyled = styled.div`
  // background-color: #9ca1ae;
  position: absolute;
  top: 140px;
  padding: 25px;
  height: auto;
  width: 100%;
  padding-left: 72px;
  // padding-top: 170px;

  .action-class {
    padding: 2px;
  }
  .edit {
    color: #c4c417;
  }
  .delete {
    color: red;
  }

  .form-wrapper {
    background-color: #fff;
    padding: 40px;
  }
`;

function EditUser() {
  let { id } = useParams();
  const [userdata, setUserdata] = useState({
    id: "",
    name: "",
    phone: "",
    email: "",
    createdAt: "",
    updatedAt: "",
  });

  const fetechURL = `https://crud-backend-ga3x.onrender.com/userbio/${id}`;

  const updateURL = `https://crud-backend-ga3x.onrender.com/editusers/${id}`;

  useEffect(() => {
    const fetchData = async () => {
      console.log(updateURL);

      try {
        const response = await axios(fetechURL);
        // console.log(response.data);
        setUserdata(response.data);
        console.log(response);
      } catch (error) {
        console.error(error);
      }
      console.log(presentData);
    };

    fetchData();
  }, []);

  const handlechange = (e) => {
    var name = e.target.name;
    var value = e.target.value;

    setUserdata({ ...userdata, [name]: value });
    // console.log(userdata);
  };

  const handleonsubmit = async () => {
    try {
      const response = await axios.put(updateURL, userdata);
      console.log(response);
      window.location = "/";
    } catch (error) {
      console.log(error);
    }
  };

  const presentData = useRef(userdata);

  const cancelBtn = () => {
    setUserdata(presentData.current);
  };

  const donothing = () => {};

  return (
    <>
      <Header text="Edit User" />
      <DashboardStyled>
        <HomeRedirection />
        <form className="form-wrapper" onSubmit={handleonsubmit}>
          <div className="row">
            <label htmlFor="name"> Name </label>
            <input
              type="text"
              className="form-control"
              placeholder="First name"
              name="name"
              onChange={handlechange}
              value={userdata.name}
            />
          </div>
          <div className="row">
            <label htmlFor="Phone"> Phone Number </label>
            <input
              type="number"
              className="form-control"
              placeholder="Phone Number"
              name="phone"
              onChange={handlechange}
              value={userdata.phone}
            />
          </div>
          <div className="row">
            <label htmlFor="Email"> Email ID </label>
            <input
              type="email"
              className="form-control"
              placeholder="Email ID"
              name="email"
              onChange={handlechange}
              value={userdata.email}
            />
          </div>
          <div className="row">
            <label htmlFor="createdAt"> Created At </label>
            <input
              type="text"
              className="form-control"
              placeholder="Email ID"
              name="createdAt"
              value={userdata.createdAt}
            />
          </div>
          <div className="row">
            <label htmlFor="updatedAt"> Updated At </label>
            <input
              type="text"
              className="form-control"
              placeholder="Email ID"
              name="updatedAt"
              value={userdata.updatedAt}
            />
          </div>
          <ButtonComponent
            type="submit"
            border="0"
            bcolor="blue"
            borderradius="4px"
            margin="14px"
            padding="6px 34px"
            color="#fff"
            onClick={donothing}
          >
            Save
          </ButtonComponent>
          <ButtonComponent
            type="submit"
            border="1px solid blue"
            bcolor="#fff"
            borderradius="4px"
            margin="14px"
            padding="5px 26px"
            color="blue"
            onClick={cancelBtn}
          >
            Cancel
          </ButtonComponent>
        </form>
      </DashboardStyled>
    </>
  );
}

export default EditUser;
