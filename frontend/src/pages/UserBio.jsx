import React, { useState, useEffect } from "react";
import axios from "axios";
import styled from "@emotion/styled";
import { useParams } from "react-router-dom";
import LinkRoute from "../components/common/LinkRoute";
import Delete from "./Delete";
import HomeRedirection from "../components/common/HomeRedirection";
import Header from "../components/common/Header";

const DashboardStyled = styled.div`
  // background-color: #9ca1ae;
  position: absolute;
  padding: 25px;
  height: auto;
  width: 100%;
  padding-left: 72px;
  // padding-top: 170px;
  top: 140px;

  .action-class {
    padding: 2px;
  }
  .edit {
    color: #c4c417;
  }
  .delete {
    color: red;
  }
`;

function UserBio() {
  let { id } = useParams();
  const [userdata, setUserdata] = useState({
    id: "",
    name: "",
    phone: "",
    email: "",
    createAt: "",
    updatedAt: "",
  });

  const URL = `https://crud-backend-ga3x.onrender.com/userbio/${id}`;

  useEffect(() => {
    const fetchData = async () => {
      //   console.log(URL);
      try {
        const response = await axios(URL);
        // console.log(response.data);
        setUserdata(response.data);
        console.log(response);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  return (
    <>
      <Header text="User Details" />
      <DashboardStyled>
        <HomeRedirection />
        <table class="table table-bordered">
          <tbody>
            <tr>
              <th scope="row">ID</th>
              <td>{userdata.id}</td>
            </tr>
            <tr>
              <th scope="row">Name</th>
              <td>{userdata.name}</td>
            </tr>
            <tr>
              <th scope="row">Phone</th>
              <td colspan="2">{userdata.phone}</td>
            </tr>
            <tr>
              <th scope="row">Email</th>
              <td colspan="2">{userdata.email}</td>
            </tr>
            <tr>
              <th scope="row">Created At</th>
              <td colspan="2">{userdata.createdAt}</td>
            </tr>
            <tr>
              <th scope="row">Updated At</th>
              <td colspan="2">{userdata.updatedAt}</td>
            </tr>
            <tr>
              <th>
                <LinkRoute to={"/editusers/" + userdata.id}>
                  <span className="material-symbols-outlined action-class edit">
                    edit
                  </span>
                </LinkRoute>
                {/* <LinkRoute to={"/editusers/" + userdata.id}>
                <span className="material-symbols-outlined action-class delete">
                  delete
                </span>
              </LinkRoute> */}
                <Delete
                  id={userdata.id}
                  className="material-symbols-outlined action-class delete"
                />
              </th>

              <td></td>
            </tr>
          </tbody>
        </table>
      </DashboardStyled>
    </>
  );
}

export default UserBio;
