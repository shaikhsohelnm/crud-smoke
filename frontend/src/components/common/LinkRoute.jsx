import React from "react";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const LinkStyled = styled(Link)({
  // backgroundColor: "#FEAF00",
  // padding: "14px 26px",
  // fontsize: "14px",
  // borderRadius: "12px",
});

function LinkRoute({
  link = "",
  className = "",
  children,
  color = "",
  padding = "",
  margin = "",
  fontsize = "",
  to = "",
  backgroundColor = "",
  onClick = "",
}) {
  return (
    <LinkStyled
      href={link}
      fontsize={fontsize}
      className={className}
      color={color}
      padding={padding}
      margin={margin}
      to={to}
      backgroundColor={backgroundColor}
      onClick={onClick}
    >
      {children}
    </LinkStyled>
  );
}

export default LinkRoute;
