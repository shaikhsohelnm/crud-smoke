import React from "react";
import Label from "./Label";
import FormInputs from "./form/FormInputs";
import LinkRoute from "./LinkRoute";
import ButtonComponent from "./ButtonComponent";
import styled from "@emotion/styled";

const DashboardStyled = styled.div`
  // background-color: #9ca1ae;
  // padding: 25px;
  width: 100%;
  height: auto;
  padding-left: 72px;
  // padding-top: 170px;
  position: absolute;
  top: 78px;
`;

const ComponentWrapper = styled.div`
  position: fixed;
  // background-color: #9ca1ae;
  padding-right: 99px;
  width: 100%;
  // padding-left: 72px;
  // padding-top: 77px;
  z-index: 1040;

  .insider-container {
    background: grey;
    padding: 20px;
    margin: 0;
    width: 100%;
  }
  .create-link {
    width: 20%;
  }
  .box {
    width: 74%;
    height: 29px;
    background-color: white;
    border-radius: 30px;
    display: flex;
    align-items: center;
    padding: 0 16px;
    margin-right: 20px;
  }

  .box > i {
    font-size: 20px;
    color: #777;
  }

  .box > input {
    width: inherit;
    flex: 1;
    height: 25px;
    border: none;
    outline: none;
    font-size: 18px;
    padding-left: 10px;

    ::placeholder {
      font-style: italic;
      font-size: 15px;
    }
  }

  .pageheading {
    width: 50%;
  }
  .search-bar {
    width: 50%;
  }
  .iniside {
    box-shadow: 0 10px 25px -24px rgba(0, 0, 0, 0.75);
  }

  .material-symbols-outlined {
    font-size: 30px;
    color: #fff;
    display: flex;
  }
`;

const Header = ({ text }) => {
  return (
    <>
      <DashboardStyled>
        <ComponentWrapper className="">
          <div className="insider-container  ">
            <div className="iniside d-flex justify-content-between w-100 align-items-center flex-wrap">
              <div className="pageheading">
                <Label
                  contentHeading
                  color="#fff"
                  content={text}
                  fontsize="30px"
                  margin="0"
                />
              </div>

              <div className=" d-flex justify-content-between align-items-center search-bar flex-wrap">
                <div class="box">
                  <i class="fa-brands fa-searching"></i>
                  <input type="text" name="" placeholder="search" />
                </div>

                <LinkRoute to="/create" className="create-link">
                  <span class="material-symbols-outlined">add_circle</span>
                </LinkRoute>
              </div>
            </div>
          </div>
        </ComponentWrapper>
      </DashboardStyled>
    </>
  );
};

export default Header;
